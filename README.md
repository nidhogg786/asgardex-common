# ASGARDEX Common

Mono repo for shared sources (utilities, clients, etc.) needed by `Asgardex` clients. Currently used by [`BEPSwap`](https://gitlab.com/thorchain/bepswap/bepswap-react-app) and [`Asgard Wallet`](https://gitlab.com/thorchain/asgard-wallet).

## Packages

- [`@thorchain/asgardex-binance`](packages/asgardex-binance/) - Custom Binance client
- [`@thorchain/asgardex-utils`](packages/asgardex-utils/) - Misc. utitity helpers
- [`@thorchain/asgardex-theme`](packages/asgardex-theme/) - Global theme for asgardex UI
