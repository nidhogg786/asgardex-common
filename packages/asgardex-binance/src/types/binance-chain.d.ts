/**
 * Types for Binance SDK
 *
 * Note:
 * Currently there are no TS types for Binance JS SDK.
 * So we have to define by ourself or to generate these later
 * See "TS types support for TS apps" https://github.com/binance-chain/javascript-sdk/issues/194
 * */
declare module '@binance-chain/javascript-sdk'
