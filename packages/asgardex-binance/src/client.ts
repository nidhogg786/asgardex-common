import bncClient from '@binance-chain/javascript-sdk'
import { BncClient, Address, MultiTransfer, Market, Balance, Network, TransferResult } from './types/binance'

export const MISSING_NETWORK_ERROR = new Error(`Network has to be defined by 'client.init(network)' before`)

export const getBinanceUrl = (network: Network): string =>
  network === Network.TESTNET ? 'https://testnet-dex.binance.org' : 'https://dex.binance.org'

export const getExplorerUrl = (network: Network): string =>
  network === Network.TESTNET ? 'https://testnet-explorer.binance.org' : 'https://explorer.binance.org'

export const getPrefix = (network: Network): string => (network === Network.TESTNET ? 'tbnb' : 'bnb')

/**
 * Interface for custom Binance client
 */
export interface BinanceClient {
  getBalance(address: Address): Promise<Balance>
  isTestnet(): boolean
  multiSend(address: Address, transactions: MultiTransfer[], memo?: string): Promise<TransferResult>
  transfer(
    fromAddress: Address,
    toAddress: Address,
    amount: number,
    asset: string,
    memo?: string,
  ): Promise<TransferResult>
  setPrivateKey(privateKey: string): Promise<BinanceClient>
  removePrivateKey(): Promise<void>
  isValidAddress(address: Address): Promise<boolean>
  getMarkets(limit?: number, offset?: number): Promise<Market>
}

/**
 * Custom Binance client
 *
 * @example
 * ```
 * import { binance } from 'asgardex-common'
 *
 * # testnet
 * const client = await binance.client(binance.Network.TESTNET)
 * await client.transfer(...)
 * # mainnet
 * const client = await binance.client(binance.Network.MAINNET)
 * await client.transfer(...)
 *
 * ```
 *
 * @class Binance
 * @implements {BinanceClient}
 */
class Client implements BinanceClient {
  private network: Network | null = null

  private bncClient: BncClient | null = null

  isTestnet = (): boolean => this.network === Network.TESTNET

  /**
   * Sets initial values
   *
   * Note: Before any other function can be used,
   * `init()` has to be called once to set initial values for
   * `network`
   */
  init = async (network: Network = Network.TESTNET): Promise<BinanceClient> => {
    if (network === Network.MAINNET) {
      this.network = Network.MAINNET
    } else {
      this.network = Network.TESTNET
    }
    await this.getClient()
    return this
  }

  private initClient = async (): Promise<BncClient> => {
    if (!this.network) {
      return Promise.reject(MISSING_NETWORK_ERROR)
    } else {
      const client = new bncClient(getBinanceUrl(this.network))
      client.chooseNetwork(this.network)
      await client.initChain()
      return client
    }
  }

  private getClient = async (): Promise<BncClient> => {
    // If a client has not been created yet,
    // a new client will be instantiated, but w/o using a private key.
    // If you do need a private key to add, call `setPrivateKey` before

    if (!this.bncClient) {
      try {
        const client = await this.initClient()
        this.bncClient = client
        return client
      } catch (error) {
        return error
      }
    } else {
      return Promise.resolve(this.bncClient as BncClient)
    }
  }

  /**
   * Sets a private key to the client
   */
  setPrivateKey = async (privateKey: string): Promise<BinanceClient> => {
    try {
      const client = await this.getClient()
      await client.setPrivateKey(privateKey)
      return this
    } catch (error) {
      return error
    }
  }

  /**
   * Removes a private key from client by creating a new client w/o a private key
   * We can not use the undocumented reference of `bncClient.privateKey` to remove key,
   * since `setPrivateKey` won't work anymore
   * There might be a better solution in the future: https://github.com/binance-chain/javascript-sdk/pull/254
   */
  removePrivateKey = async (): Promise<void> => {
    try {
      this.bncClient = await this.initClient()
    } catch (error) {
      return error
    }
  }

  isValidAddress = async (address: Address): Promise<boolean> => {
    const client = await this.getClient()
    if (!this.network) {
      // In theory that should never never happen
      return Promise.reject(MISSING_NETWORK_ERROR)
    } else {
      return client.checkAddress(address, getPrefix(this.network))
    }
  }

  getBalance = async (address: Address): Promise<Balance> => {
    const client = await this.getClient()
    return client.getBalance(address)
  }

  getMarkets = async (limit = 1000, offset = 0): Promise<Market> => {
    const client = await this.getClient()
    return client.getMarkets(limit, offset)
  }

  multiSend = async (address: Address, transactions: MultiTransfer[], memo = '') => {
    const client = await this.getClient()
    const result = await client.multiSend(address, transactions, memo)
    await this.removePrivateKey()
    return result
  }

  transfer = async (fromAddress: Address, toAddress: Address, amount: number, asset: string, memo = '') => {
    const client = await this.getClient()
    const result = await client.transfer(fromAddress, toAddress, amount, asset, memo)
    await this.removePrivateKey()
    return result
  }
}

export const client = new Client().init
