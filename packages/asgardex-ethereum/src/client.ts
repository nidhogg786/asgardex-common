import { ethers } from 'ethers'
const providers = ethers.providers
const Wallet = ethers.Wallet
const utils = ethers.utils

import { TransactionResponse } from 'ethers/providers'

import { generateMnemonic } from 'bip39'
import { BigNumberish } from 'ethers/utils'

/**
 * Interface for custom Ethereum client
 */
export interface EthereumClient {
  connect(network: string): ethers.Wallet
  getBalance(address: string): Promise<BigNumberish>
  getBlockNumber(): Promise<number>
  getHistory(address: string): Promise<TransactionResponse[]>
  sendTransaction(to: string, data: string, value: BigNumberish): Promise<TransactionResponse[]>
}

/**
 * Custom Ethereum client
 * @todo Error handling
 */
export default class Client implements EthereumClient {
  public Wallet: any | null = null

  /**
   * Creates an ethereum wallet with optional mnemonic
   */
  init = (mnemonic: string | null = null): EthereumClient => {
    const seed = mnemonic ? mnemonic : generateMnemonic()
    this.Wallet = Wallet.fromMnemonic(seed)
    return this
  }

  /**
   * Connects an existing wallet to a provider eg. etherscan
   * @todo handle backup providers
   */
  connect = (network: string): ethers.Wallet => {
    const provider = new providers.EtherscanProvider(network)
    this.changeWallet(this.Wallet.connect(provider))
    return this.Wallet
  }

  /**
   * Gets the eth balance of an address
   * @todo add start & end block parameters
   */
  getBalance = async (address: string): Promise<BigNumberish> => {
    const etherString = await this.Wallet.provider.getBalance(address)
    return utils.formatEther(etherString)
  }

  /**
   * Gets the current block of the network
   */
  getBlockNumber = async (): Promise<number> => {
    const block = await this.Wallet.provider.getBlockNumber()
    return block
  }

  /**
   * Gets the history of an address.
   */
  getHistory = async (address: string): Promise<TransactionResponse[]> => {
    const history = await this.Wallet.provider.getHistory(address)
    return history
  }

  /**
   * Sends a transaction
   * @todo add from?: string, nonce: BigNumberish, gasLimit: BigNumberish, gasPrice: BigNumberish, chainId: number
   */
  sendTransaction = async (to: string, data: string, value: BigNumberish): Promise<TransactionResponse[]> => {
    const transactionRequest = { to: to, value: value, data: Buffer.from(data, 'utf8') }
    const transactionResponse = await this.Wallet.sendTransaction(transactionRequest)
    return transactionResponse
  }

  /**
   * changes the wallet eg. when using connect() after init()
   */
  private changeWallet = (Wallet: ethers.Wallet): ethers.Wallet => {
    return (this.Wallet = Wallet)
  }
}
