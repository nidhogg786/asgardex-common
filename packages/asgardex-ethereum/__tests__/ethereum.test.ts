import Client from '../src/client'
import { Wallet, providers } from 'ethers'

const mockWallet = {
  signingKey: {
    address: '0xb8c0c226d6FE17E5d9132741836C3ae82A5B6C4E',
    keyPair: {
      compressedPublicKey: '0x02ef84375983ef666afdf0e430929574510aa56fb5ee0ee8c02a73f2d2c12ff8f7',
      privateKey: '0x739172c3520ea86ad6238b4f303cc09da6ca7254c76af1a1e8fa3fb00eb5c16f',
      publicKey:
        '0x04ef84375983ef666afdf0e430929574510aa56fb5ee0ee8c02a73f2d2c12ff8f7eee6cdaf9ab6d14fdeebc7ff3d7890f5f98376dac0e5d816dca347bc71d2aec8',
      publicKeyBytes: [
        2,
        239,
        132,
        55,
        89,
        131,
        239,
        102,
        106,
        253,
        240,
        228,
        48,
        146,
        149,
        116,
        81,
        10,
        165,
        111,
        181,
        238,
        14,
        232,
        192,
        42,
        115,
        242,
        210,
        193,
        47,
        248,
        247,
      ],
    },
    mnemonic: 'canyon throw labor waste awful century ugly they found post source draft',
    path: "m/44'/60'/0'/0/0",
    privateKey: '0x739172c3520ea86ad6238b4f303cc09da6ca7254c76af1a1e8fa3fb00eb5c16f',
    publicKey:
      '0x04ef84375983ef666afdf0e430929574510aa56fb5ee0ee8c02a73f2d2c12ff8f7eee6cdaf9ab6d14fdeebc7ff3d7890f5f98376dac0e5d816dca347bc71d2aec8',
  },
}

describe('Ethereum Client', () => {
  it('should return a new wallet', () => {
    const EthClient = new Client()
    EthClient.init()

    expect(EthClient.Wallet).toBeInstanceOf(Wallet)

    expect(EthClient.Wallet.signingKey).toEqual(
      expect.objectContaining({
        address: expect.any(String),
        keyPair: expect.any(Object),
        mnemonic: expect.any(String),
        path: expect.any(String),
        privateKey: expect.any(String),
        publicKey: expect.any(String),
      }),
    )
  })

  it('should return a wallet from mnemonic', () => {
    const mnemonic = 'canyon throw labor waste awful century ugly they found post source draft'
    const EthClient = new Client()
    EthClient.init(mnemonic)

    expect(EthClient.Wallet).toBeInstanceOf(Wallet)
    expect(EthClient.Wallet.provider).toBe(undefined)
    expect(EthClient.Wallet.signingKey).toMatchObject(mockWallet.signingKey)
  })

  it('should connect', () => {
    const mnemonic = 'canyon throw labor waste awful century ugly they found post source draft'
    const EthClient = new Client()
    EthClient.init(mnemonic)
    EthClient.connect('rinkeby')

    expect(EthClient.Wallet).toBeInstanceOf(Wallet)
    expect(EthClient.Wallet.provider).toBeInstanceOf(providers.EtherscanProvider)
    expect(EthClient.Wallet.signingKey).toMatchObject(mockWallet.signingKey)
  })
})
