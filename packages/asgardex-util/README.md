# `@thorchain/asgardex-util`

## Modules

- `bn` - Utitilies for using `bignumber.js`
- `async` - Utitilies for `async` handling

## Installation

```
yarn add @thorchain/asgardex-util
```

Following peer dependencies have to be installed into your project. These are not included in `@thorchain/asgardex-util`.

```
yarn add bignumber.js
```
