import dark from './dark'
import light from './light'

export * from './types'

export default {
  dark,
  light,
}
